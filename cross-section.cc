
/// simple program to get a quick estimate of cross sections and a
/// handful of basic distributions from MC run and flat/HepMC/UW
/// files, etc.
///
#include "AnalysisFramework.hh"
#include "boost/foreach.hpp"
#define foreach BOOST_FOREACH


using namespace std;
using namespace fastjet;

double log10(double x) {return log(x)/log(10.0);}

/// Example class derived from AnalysisFramework that will help in evaluation of
/// some basic cross sections. 
class XSctAnalysis : public AnalysisFramework {
public:
  XSctAnalysis(CmdLine & cmdline) : AnalysisFramework(cmdline) {}
  void user_startup() {
    // extra user parameters
    param["missing.ptmin"] = 30.0;
    param["jet.ptmin"] = 20.0;
    param["jet.rapmax"] = 5.0;

    jet_def = JetDefinition(aachen_algorithm, cmdline.value<double>("-R",1.0));

    DefaultHist::set_defaults(0.0, 4.4, cmdline.value("-hist.binwidth",0.2));

    hists_2d["lund-zrel"].declare(0.0, 5.0, 0.2, -10.0, 0.0, 0.2);
    hists_2d["lund-zabs"].declare(0.0, 5.0, 0.2, -10.0, 0.0, 0.2);
    hists_2d["lund-lnpt"].declare(0.0, 5.0, 0.2,  -5.0, 7.0, 0.2);
  }

  void analyse_event() {
    // cout << "Cross section pointer: " 
    // 	 << driver->generator->gen_event()->cross_section()  << " "
    // 	 << driver->generator->gen_event()->weights().size()
    // 	 << endl;

    double evwgt = driver->generator->hadron_level().weight();
    xsections["total cross section"] += evwgt;

    // // temporary for Ken Lane: muon rapidity
    // auto muons = (SelectorNHardest(2)*SelectorAbsPDGId(13))(driver->generator->hadron_level().particles());
    // hists["mupair.rap"].set_lims_add_entry(-10.0, 10.0, 1.0, (muons[0]+muons[1]).rap(),evwgt);
    // hists["mupair.eta"].set_lims_add_entry(-10.0, 10.0, 1.0, (muons[0]+muons[1]).eta(),evwgt);

    auto particles  = driver->generator->hadron_level().particles();
    
    averages["event multipliticity"] += evwgt * driver->generator->hadron_level().particles().size();

    auto jets = SelectorNHardest(2)(jet_def(particles));

    for (const auto & j: jets) {
      PseudoJet jj, j1, j2;
      jj = j;
      while (jj.has_parents(j1,j2)) {
        // make sure j1 is always harder branch
        if (j1.pt2() < j2.pt2()) swap(j1,j2);

        // collect info and fill in the histogram
        double delta_R = j1.delta_R(j2);
        double delta_R_norm = delta_R / jet_def.R();
        double z = j2.pt()/(j1.pt() + j2.pt());
        double y = log(1.0 / delta_R_norm);

        // there is an ambiguity here: can use z or j2.pt() / j.pt()
        double lnpt_rel = log(z * delta_R_norm);
        double lnpt_abs = log(j2.pt()/j.pt() * delta_R_norm);
        
        hists_2d["lund-zrel"].add_entry(y, lnpt_rel, evwgt);
        hists_2d["lund-zabs"].add_entry(y, lnpt_abs, evwgt);

        double lnpt = log(j2.pt() * delta_R);
        hists_2d["lund-lnpt"].add_entry(y, lnpt, evwgt);
        // follow harder branch
        jj = j1;
      }
      
    }
  }
};

//----------------------------------------------------------------------
int main (int argc, char ** argv) {
  
  CmdLine cmdline(argc,argv);
  XSctAnalysis analysis(cmdline);
  analysis.run();
}
