MC study of Lund plane through jet declustering 
===============================================


This repository illustrates one way of investigating the pattern of
emissions inside a jet, by plotting the primary-emission Lund plane from
a sample of jets. 

It was prepared in the context of the CERN TH Department institute,
["Novel tools and observables for jet physics in heavy-ion collisions /
5th Heavy Ion Jet
Workshop"](https://indico.cern.ch/event/625585/overview) to introduce a
way of probing modifications of the pattern of emissions in a relatively
theory-agnostic manner.

Method
------


The analysis starts with a jet clustered with the Cambridge-Aachen
algorithm (it could also be an anti-kt jet that has been reclustered).
It iterates the following steps, starting from a jet $`j`$.

1. Set $`j_0 = j`$

2. Undo the last clustering of $`j_0`$, to give two pieces $`j_1`$,$`j_2`$
   such that $`p_{t,j_1} > p_{t,j_2}`$

3. Register the kinematics of $`j_2`$ in the Lund plane (cf. below)

4. If $`j_1`$ consists of a single particle, stop. Otherwise reset 
   $`j_0 = j_1`$ and repeat from step 1

The Lund plane involves two variables: 

  - the horizontal axis is $`\log(R / \Delta R_{12})`$, where $`R`$ is the
    radius of the jet clustering definition and $`\Delta R_{12}`$ is the
    separation between the two subjets $`j_1`$,$`j_2`$
  
  - the vertical axis is the logarithm of some pt-like quantity. 
  
For the $`p_t`$-like quantity, the associated code illustrates three possible choices:
  
  1. Define $`z_{rel} = p_{t,j_2}/(p_{t,j_1} + p_{t,j_2})`$ and use
     $`\log(z_{rel} \Delta R_{12}/R)`$. This definition of $`z`$ has the
     property that it always reflects the momentum sharing within that
     specific declustering.
  
  2. Define $`z_{abs} = p_{t,j_2}/p_{t,j}`$ and use 
     $`\log(z_{abs} \Delta R_{12}/R)`$. This definition of $`z`$ has the the property
     that it more accurately reflects the transverse momentum of the emission relative to the jet $`p_t`$.

  3. Use $`\log(p_{t,j_2} \Delta R_{12}/ \text{GeV})`$. This definition
     has the advantage that it corresponds to an absolute momentum
     scale, which is useful for judging where an emission is situated relative to the non-perturbative boundary (positions around 0 in this variable).

The density of coverage of the Lund plane should, to a first
approximation, be given by 

```math
  2 C \frac{\alpha_s}{\pi} 
```
where C is the effetive colour factor, some weighted average of
$`C_F=4/3`$ and $`C_A=3`$.


Code and results
----------------

Sample code to generate these results (which runs with my internal MC
framework) is given by [cross-section.cc](cross-section.cc). The code
should be readable independently of the framework. It analyses the two
hardest jets in an event, relying on the fact that the MC generator has
been run for the dijet process with a pt generation cut, so those two
jets are sensible.

Sample results are given in the file [plot.pdf](plot.pdf) for different
$`p_t`$ cuts and $`R`$ choices. 

Unless otherwise specified in the plot title, results are at hadron
level with the UE turned off. They are for pp collisions at 14 TeV.

