# gnuplot

reset
set term pdfcairo size 8cm,8cm
set output 'plot.pdf'

set xlabel 'ln (R / ΔR)'

set palette model RGB
set palette defined (0 1 1 1, 0.5 1 0.2 0 , 1 1.0 0.8 0 , 1.5 0.5 0.8 0, 2 0 0.8 0, 3 0 1 1, 5 0 0 1)

#set palette defined (0 0 0 0, 1.0 0.5 0.0 0.0)

set xrange [0:]
set cbrange [0:1]



do for [file in 'py8-R1.0-ptmin500.dat py8-R0.4-ptmin200.dat py8-R0.4-ptmin200-parton.dat'] {
  #file='py8-R1-ptmin500.dat'
  #file='py8-R0.4-ptmin200.dat'
  #file='py8-R0.4-ptmin200-parton.dat'
  xsc=system("grep total.cross.section ".file."| awk '{print $7}'")
  # two jets per event (as per analysis)
  jetxsc = 2*xsc 
  #
  binsize = system("mergeidx.pl -f py8-R0.4-ptmin200.dat lund-zrel | head -3 | tail -1 | awk '{print ($3-$1)*($6-$4)}'")
  print binsize
  print xsc

  set yrange [:0]
  set ylabel 'ln z ΔR / R'
  set title file.' z_{rel}'
  plot '<mergeidx.pl -f '.file.' lund-zrel' u 2:5:($7/jetxsc) w image t ''
  set title file.' z_{abs}'
  plot '<mergeidx.pl -f '.file.' lund-zabs' u 2:5:($7/jetxsc) w image t ''

  set auto y
  set ylabel 'ln (p_t ΔR/GeV)'
  set title file.' ln (p_t ΔR/GeV)'
  plot '<mergeidx.pl -f '.file.' lund-lnpt' u 2:5:($7/jetxsc) w image t ''
  
}

set output